# EX 4
# File: geometry_class.py

# classes and object oriented

import math

# triangle

class Triangle:
	def __init__(self): # required
		self.area = 0 # cannot be empty
		self.height = 0 
		self.width = 0

	def __init__(self, height=0, width=0):
		self.height = height
		self.width = width
		self.area = self.getArea()

	def getArea(self): # now class-dependent 
		print "....running getArea/n"
		return self.height * self.width/2

tr=Triangle()
print tr.height

tr.height = 2
tr.width = 4
print tr.area # the area is not changed 
print tr.getArea()

tr2 = Triangle(4,6)
print tr2.getArea()
print tr2.area

# circle

class Circle:
	def __init__(self, radius = 0):
		self.radius = radius
		self.area = self.getArea()
		self.perimeter = self.getPerimeter()

	def getArea(self):
		print "....running getArea/n"
		return math.pi * self.radius**2 

	def getPerimeter(self):
		return 2 * math.pi * self.radius

c1 = Circle(1)
print c1.area
print c1.perimeter
